console.log(window.location.search);

let params = new URLSearchParams(window.location.search);
let adminUser = localStorage.getItem("isAdmin");

console.log(params.has('courseId'));

console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

if (adminUser == "false" || !adminUser) {
	alert("You don't have administrator rights to this page!")
	window.location.replace("./courses.html")
}
	

	fetch(`https://fierce-wave-76572.herokuapp.com/api/courses/${courseId}`, {
		method: 'PUT',
		headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			courseId: courseId,
			isActive: true
		})

	})
	.then(res => res.json())
	.then(data => {

		console.log(data);

		if(data === true){

			alert("Course successfully enabled.");
			window.location.replace("./courses.html");
		
		} else {

			alert("something went wrong.");

		}
	})
let navItems = document.querySelector("#navSession");
let registerBtn = document.querySelector("#registerBtn");
// let navItem1 = document.querySelector("#navItem1");

let userToken = localStorage.getItem("token");
console.log(userToken);

if (!userToken) {

	navItems.innerHTML = 
		`
			<li class="nav-item">
					<a href="./login.html" class="nav-link"> Log In </a>
			</li>	

		`

	registerBtn.innerHTML = 
		`
			<li class="nav-item">
					<a href="./register.html" class="nav-link"> Register </a>
			</li>	

		`	
} else {


	navItems.innerHTML = 
		`
			<li class="nav-item">
					<a href="./logout.html" class="nav-link nav-scroll"> Log Out </a>
			</li>	


		`	

}
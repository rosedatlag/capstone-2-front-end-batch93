let token = localStorage.getItem('token');

if(!token || token === null) {

	// Unauthenticated user
	alert('You must login first');
	// Redirect user to login page
	window.location.href="./login.html";

} else {

				fetch('https://fierce-wave-76572.herokuapp.com/api/users/details', {
					headers: {
						'Authorization': `Bearer ${token}`
					}
				})
				.then(res => res.json())
				.then(data => {

					console.log(data);

					document.getElementById('firstName').value = data.firstName;
					document.getElementById('lastName').value = data.lastName;
					document.getElementById('emailAdd').value = data.email;
					document.getElementById('mobileNo').value = data.mobileNo;

					let formSubmit = document.querySelector("#editProfile");
					formSubmit.addEventListener("submit", (e) => {

						e.preventDefault();

						let firstName = document.querySelector("#firstName").value;
						let lastName = document.querySelector("#lastName").value;
						let emailAdd = document.querySelector("#emailAdd").value;
						let mobileNo = document.querySelector("#mobileNo").value;

						fetch('https://fierce-wave-76572.herokuapp.com/api/users/edit', {
							method: 'PUT',
							headers: {
								'Content-Type': 'application/json',
								'Authorization': `Bearer ${token}`
							},
							body: JSON.stringify({

								firstName: firstName,
								lastName: lastName,
								email: emailAdd,
								mobileNo: mobileNo
							})
						})
						.then(res => res.json())
						.then(data => {

							console.log(data)

							if (data === true) {

								window.location.replace("./profile.html")
							} else {

								alert("Something went wrong!")
							}
						})

					})

	})
}
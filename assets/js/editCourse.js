console.log(window.location.search);

let params = new URLSearchParams(window.location.search);

let adminUser = localStorage.getItem("isAdmin");

console.log(params.has('courseId'));


console.log(params.get('courseId'));

let courseId = params.get('courseId');

let token = localStorage.getItem('token');

if (adminUser == "false" || !adminUser) {
	alert("You don't have administrator rights to this page!")
	window.location.replace("./courses.html")
}

fetch(`https://fierce-wave-76572.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data);

	document.getElementById('courseName').placeholder = data.name;
	document.getElementById('courseDescription').placeholder = data.description;
	document.getElementById('coursePrice').placeholder = data.price;

	let formSubmit = document.querySelector("#editCourse");
	formSubmit.addEventListener("submit", (e) => {

		e.preventDefault();

		let courseName = document.querySelector("#courseName").value;
		let courseDescription = document.querySelector("#courseDescription").value;
		let coursePrice = document.querySelector("#coursePrice").value;

		fetch('https://fierce-wave-76572.herokuapp.com/api/courses', {
			method: 'PUT',
			headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
					courseId: courseId,
					name: courseName,
					description: courseDescription,
					price: coursePrice
			})

		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			if(data === true){

				alert("Course successfully modified.");
				window.location.replace("./courses.html");

			} else {

				alert("something went wrong");
			}
		})
	})
})